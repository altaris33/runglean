# runglean

# Repo for the runglean upcoming app

## Database architecture

- techno: PostgreSQL 11.9 (Debian)

## Backend architecture

- Java Spring Boot
- Java Spring Data (JPA)

## Frontend architecture

- Angular 10

## Interchange standard

- JSON